var search_view_class = "col-sm-4";  
var View = (function(){

	function View(){

	};
	View.prototype.changeView = function(){
		

		if($('#movie-list').hasClass('list-inline')){
			$('#movie-list').removeClass('list-inline');
			$('#movie-list').addClass('list-group');
			$('.item').removeClass('col-sm-4');
		}else{
			$('#movie-list').removeClass('list-group');
			$('#movie-list').addClass('list-inline');
			$('.item').addClass("col-sm-4");;
			
		}
		
	};

	
	View.prototype.changeSearchType = function(){
		var type = $('#view-btn-form').text();
		console.log(type)
		if(type == "Discover"){
			$('#view-btn-form').text('Search');
			$('#movie-form-search').addClass("inactive");
			$('#movie-form-discover').removeClass("inactive");
		}else if(type == "Search"){
			$('#view-btn-form').text('Discover');
			$('#movie-form-search').removeClass("inactive");
			$('#movie-form-discover').addClass("inactive");
		}
	}

	View.prototype.add = function(movie,index){
		set(movie);
		$('.new-item').find('button').attr("onclick", "view.addFavorite("+JSON.stringify(result[index])+")");
		$('.new-item')
			.clone()
			.removeClass('new-item')
			.removeClass('inactive')
			.addClass(search_view_class)
			.prependTo('#movie-list');

	}
	View.prototype.addFavorite = function(self){
		$('.msg').remove();
		var movie = new Movie(self);
		set(movie);
		$('.new-item')
			.clone()
			.removeClass('new-item')
			.removeClass('inactive')
			.addClass('col-sm-4')
			.prependTo('#favorite-movies')
			.find('button').remove();
			$('#favorite-movies').show();
		$("html, body").animate({ scrollTop: 0 }, "slow");


	}

	function set(movie){
		$('.new-item').find('h3').text(movie.getTitle());
		$('.new-item').find('h4').text(movie.getDate());
		$('.new-item').find('.image').attr('src', movie.getImage());
		$('.new-item').find('.overview').text(movie.getOverview());
		$('.new-item').find('.votes').find('b').text(movie.getVote());
		$('.new-item').find('.popularity').find('b').text(movie.getPopularity());
		$('.new-item').find('.genre span').text(movie.getGenders())
	}
	View.prototype.toggleFavorite = function(){
		$('.msg').remove();
		if($('#favorite-movies').children().length > 1){
			$('#favorite-movies').toggle('slow');
		}else{
			$('<p class="msg text-center">Няма добавени филми</p>').insertBefore('#favorite-movies' )
		}
		
	}

	
	return View;
})();

var view = new View();