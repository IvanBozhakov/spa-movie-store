var Movie = (function(){
	var _id = null;
	var _title = null;
	var _release_date = null;
	var _overview = null;
	var _poster_path = null;
	var _original_language = null;
	var _vote = null;
	var _popularity = null;
	var _movie;
	var _genres = null;
	function Movie(movie){
		_id = movie.id;
		_title = movie.title;
		_release_date = movie.release_date;
		_overview = movie.overview;
		_original_language = movie.original_language;
		_vote = movie.vote_count;
		_poster_path = movie.poster_path;
		_popularity = movie.popularity;
		_genres = movie.genre_ids;

	};

	Movie.prototype.getId = function(){
		return _id;
	}

	Movie.prototype.getTitle = function(){
		return _title;
	}

	Movie.prototype.getDate = function(){
		return _release_date;
	}

	Movie.prototype.getOverview= function(){
		return _overview;
	}

	Movie.prototype.getLanguage= function(){
		return _original_language;
	}
	
	Movie.prototype.getVote = function(){
		return _vote;
	}
	Movie.prototype.getPopularity= function(){
		return Math.round(_popularity);
	}

	Movie.prototype.getImage = function(){
		if(_poster_path != null)
			return "https://image.tmdb.org/t/p/w500"+_poster_path;
		return "#"
	}
	Movie.prototype.getGenders = function(){
		var genres_list = "";
		for(var i  = 0;i<_genres.length;i++){
			if(i == _genres.length - 1){
				genres_list += genders[_genres[i]]+" "
			}else{
				genres_list += (genders[_genres[i]]+", ")
			}
			
		}
		return genres_list;
	}



	return Movie;

})();