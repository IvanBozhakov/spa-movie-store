var result = [];
var genders = {};


  var Api = (function(){
  	function Api(){

  	};

  	var api_key = "9803522fca2e8e20f9120d8764e84b9f";
  	var url = "https://api.themoviedb.org/3/";
  	/*
  	*@name
  	*@type [movie, tv/season]
  	*/
  	Api.prototype.search = function(){
  		var query = $('#search_name').val();
  		var year = $('#year-search').find(":selected").val();
  		if(query != "" && year != ""){
			request({api_key: api_key, query: query, year: year}, "search/movie");
  		}else{
  			//error message
  		} 
  		
  	}

  	Api.prototype.discover = function(){
  		var genre = $('#genres').find(":selected").val();
  		var sort = $('#sort').find(":selected").val();
  		var year = $('#year-discover').find(":selected").val();

  		
  			request({api_key: api_key, with_genres: genre, sort_by: sort, primary_release_year: year}, "discover/movie");
  		
  		
  	}

  	function request(param, type){
  		$.get(url+type, param)
		  .done(function( data ) {
		  	//console.log(data.results);
		   response(data);
		});
  	}

  	Api.prototype.genresList = function(){
  		$.get(url+"genre/movie/list", {api_key: api_key})
		  .done(function( data ) {
        var arr = data.genres;
		  	for(var i = 0;i<arr.length;i++){
          genders[arr[i].id] = arr[i].name;
		  		$('#genres').append('<option value='+arr[i].id+'>'+arr[i].name+'</option>')
		  	} 
		   		
		});
  	}

  	function response(data){
  		result = [];
  		for(var i = data.results.length-1; i>=0;i--){

        result.push(data.results[i]);
  			view.add(new Movie(data.results[i]),i);
  		}
  	}



  	

  	return Api;
  })();

  var api = new Api();


$(document).ready(function(){
	//add genres list
	api.genresList();
});